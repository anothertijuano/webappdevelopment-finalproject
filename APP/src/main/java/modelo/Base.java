package modelo;


import java.lang.reflect.Array;
import java.util.ArrayList;

public class Base {

    public Base() {
        System.out.println("Aquí pondría mi base de datos... Si tuviera una!");
    }

    public Cliente getClienteByID( int id ){
        System.out.println("Simón S, ahí te mando al cliente, no hay fijón");
        Cliente tmp;
        tmp = new Cliente(666,"Daniel","Miramontes","mail@mail.app",665309826, 456.34);
        return (tmp);
    }

    public ArrayList<Cliente> getAllClientes(){
        ArrayList<Cliente> clientes=new ArrayList<Cliente>();
        Cliente tmp;
        tmp = new Cliente(661,"Daniel","NotTheCool","mail1@mail.app",665309821, 456.31);
        clientes.add(tmp);
        tmp = new Cliente(662,"Daniel","TheUncool","mail2@mail.app",665309822, 456.32);
        clientes.add(tmp);
        tmp = new Cliente(663,"Daniel","TheCool","mail3@mail.app",665309823, 456.33);
        clientes.add(tmp);
        tmp = new Cliente(664,"Daniel","TheCooler","mail4@mail.app",665309824, 456.34);
        clientes.add(tmp);
        tmp = new Cliente(665,"Daniel","TheCoolest","mail5@mail.app",665309825, 456.35);
        clientes.add(tmp);
        return (clientes);
    }
}

