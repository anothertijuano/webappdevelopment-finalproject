package modelo;

public class Cliente {
    private  int id;
    private String nombre;
    private String apellido;
    private String email;
    private int tel;
    private double saldo;

    public Cliente(){}

    public Cliente(int id, String nombre, String apellido, String email, int tel, double saldo){
        this.id=id;
        this.nombre=nombre;
        this.apellido=apellido;
        this.email=email;
        this.tel=tel;
        this.saldo=saldo;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getEmail() {
        return email;
    }

    public int getTel() {
        return tel;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
