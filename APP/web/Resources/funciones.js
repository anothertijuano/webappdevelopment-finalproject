function showById(ElementId) {
    document.getElementById(ElementId).style.display = "block";
    var containerElement=document.getElementById('main');
    containerElement.setAttribute('class', 'blur');
}

function hideById(ElementId) {
    document.getElementById(ElementId).style.display = "none";
    var containerElement=document.getElementById('main');
    containerElement.setAttribute('class', 'none');
}
