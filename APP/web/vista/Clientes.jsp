<%--
  Created by IntelliJ IDEA.
  User: andrade
  Date: 6/16/20
  Time: 1:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="Resources/style.css">
    <script src="Resources/funciones.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Inicio</title>
</head>
<body>
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <h2>Clientes</h2>
        </a>
    </nav>
    <div id="main">
        <div id="btn" class="columna">
            <button type="button" class="btn btn-primary" onclick="showById('agregar')">Agregar cliente</button>
        </div>
        <div id="contenido">
            <div class="columna columna-big">
                <table class="table table-striped ">
                    <thead class="thead-dark">
                        <tr>
                            <td scope="col">#</td>
                            <td scope="col">Nombre</td>
                            <td scope="col">Saldo</td>
                            <td scope="col">Frecuencia</td>
                            <td scope="col"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="row">0</td>
                            <td>Juan Perez</td>
                            <td>1023</td>
                            <td>ALTO</td>
                            <td>
                                <a href="${pageContext.request.contextPath}/detallesCliente?id=0&action=edit">
                                    <button type="button" class="btn btn-link">Editar</button>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="columna columna-small">
                <div class="linea">
                    <div class="card alert-primary text-center">
                        <div class="card-body">
                            <h5 class="card-title">Saldo Total</h5>
                            <p class="card-text">$350.00</p>
                        </div>
                    </div>
                </div>
                <div class="linea">
                    <div class="card alert-primary text-center">
                        <div class="card-body">
                            <h5 class="card-title">Total de clientes</h5>
                            <p class="card-text">2</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="agregar" class="card">
        <h5 class="card-header">Agregar Cliente</h5>
        <div class="card-body">
            <form name="DatosCliente" method="post" action="LoginServlet">
                <div class="form-group">
                    <label for="Nombre">Nombre</label>
                    <input type="text" class="form-control" name="Nombre">
                </div>
                <div class="form-group">
                    <label for="Apellido">Apellido</label>
                    <input type="text" class="form-control" name="Apellido">
                </div>
                <div class="form-group">
                    <label for="Mail">e-mail</label>
                    <input type="email" class="form-control" name="Mail">
                </div>
                <div class="form-group">
                    <label for="Telefono">Teléfono</label>
                    <input type="tel" class="form-control" name="Telefono">
                </div>
                <div class="form-group">
                    <label for="Saldo">Saldo</label>
                    <input type="number" class="form-control" name="Saldo">
                </div>
                <input type="submit" class="btn btn-primary" value="Guardar">
                <button type="button" class="btn btn-secondary" onclick="hideById('agregar')">Cancelar</button>
            </form>
        </div>
    </div>
</body>
</html>