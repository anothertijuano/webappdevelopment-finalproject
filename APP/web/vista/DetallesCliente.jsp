<%--
  Created by IntelliJ IDEA.
  User: andrade
  Date: 6/16/20
  Time: 12:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="Resources/style.css">
    <script src="Resources/funciones.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Inicio de sesión</title>
</head>

<body>
<nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
        <h2>Clientes</h2>
    </a>
</nav>
    <div class="card" id="main">
        <h5 class="card-header">Editar detalles de Cliente</h5>
        <div class="card-body">
            <form name="DetallesCliente" method="post" action="LoginServlet">
                <div class="form-group">
                    <label for="Nombre">Nombre</label>
                    <input type="text" class="form-control" name="Nombre">
                </div>
                <div class="form-group">
                    <label for="Apellido">Apellido</label>
                    <input type="text" class="form-control" name="Apellido">
                </div>
                <div class="form-group">
                    <label for="Mail">e-mail</label>
                    <input type="email" class="form-control" name="Mail">
                </div>
                <div class="form-group">
                    <label for="Telefono">Teléfono</label>
                    <input type="tel" class="form-control" name="Telefono">
                </div>
                <div class="form-group">
                    <label for="Saldo">Saldo</label>
                    <input type="number" class="form-control" name="Saldo">
                </div>
                <input type="submit" class="btn btn-primary" value="Guardar">
                <a href="${pageContext.request.contextPath}/LoginServlet">
                    <button type="button" class="btn btn-secondary">Cancelar</button>
                </a>
                <button type="button" class="btn btn-danger">Eliminar</button>
            </form>
        </div>
    </div>
</body>
</html>
